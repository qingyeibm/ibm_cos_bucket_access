#!/bin/sh
#
show_usage()
{
  echo -----------------------------------------------------------------------
  echo To run the script:
  echo "  bucket_access <bucket_name>"
  echo
  echo Please set the two environment variables used in the script before running it:
  echo " 1.  <your_account_id> IBMCLOUD_ACCOUNT_GUID"
  echo " 2.  <your_apikey>     IBMCLOUD_API_KEY"
  echo -----------------------------------------------------------------------
}

bucket_name=$1

if [ $# -ne 1 -o -z "$IBMCLOUD_API_KEY" -o -z "$IBMCLOUD_ACCOUNT_GUID" ]
then
   show_usage
   exit 1
fi

accountID="${IBMCLOUD_ACCOUNT_GUID}"

#######################################################################
# Get the IAM token
#######################################################################
echo Fetching new cloud access token
IAM_TOKEN=$(curl --location --request POST 'https://iam.cloud.ibm.com/identity/token' \
  --header 'Accept: application/json' \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --data-urlencode 'grant_type=urn:ibm:params:oauth:grant-type:apikey' \
  --data-urlencode "apikey=${IBMCLOUD_API_KEY}" | jq -r '.access_token')

if [ "$IAM_TOKEN" = "null" -o -z "$IAM_TOKEN" ]
then
  echo Failed to retrieve IAM token
  exit 1
fi

######################################################################
# Get the COS instance service id of the bucket
######################################################################
bucket_json=$(curl https://config.cloud-object-storage.cloud.ibm.com/v1/b/${bucket_name} -H "authorization: bearer $IAM_TOKEN")

if [ "$bucket_json" = "" ] ; then
  echo "Could not find COS bucket metadata for: $bucket_name"
  exit 1
fi

instanceID=$(echo $bucket_json | jq -r ".service_instance_id")

######################################################################
# Get the policies for the COS instance
######################################################################
echo Fetching Access Policy list for COS instance: $instanceID
json_out=`curl -X GET "https://iam.cloud.ibm.com/v1/policies?account_id=$accountID&type=access&service_name=cloud-object-storage&service_instance=$instanceID" \
  -H "Authorization: Bearer ${IAM_TOKEN}" -H "Content-Type: application/json"`
if [ "$json_out" = "null" ] || [ "$json_out" = "" ]
then
  echo Failed to retrieve Access Policies
  exit 1
fi

index=0
found_bucket_policies=0
policyNo=`echo $json_out | jq '.policies' | jq 'length'`

echo ======================================================================
echo Number of policies found for COS instance $instanceID: $policyNo

if [ $policyNo -lt 1 ]
then
  echo No policy found for the COS instance
  exit 0
fi

echo List Access Policies for bucket: $bucket_name
#echo ----------------------------------------------------------------------
while [ true ]
do
   if [ $index -eq $policyNo ]
   then
      break
   fi
   # loop through policies
   resource=`echo $json_out | jq -r --arg index "$index" '.policies[$index | tonumber].resources[].attributes[] | select( .name == "resource" ) | .value'`
   accessid=`echo $json_out | jq -r --arg index "$index" '.policies[$index | tonumber].subjects[].attributes[] | .value'`
   roles=`echo $json_out | jq -r --arg index "$index" '.policies[$index | tonumber].roles[] | .display_name'`
   index=`expr $index + 1`
   accessType="Unknown"

   if [ "$resource" = "$bucket_name" ]
   then
      found_bucket_policies=`expr $found_bucket_policies + 1`

      echo ----------------------- Bucket access policy $found_bucket_policies ------------------------
      if [ "`echo $accessid | cut -d'-' -f1`" = "AccessGroupId" ]
      then
          accessType="Access Group"
          access_name=`ibmcloud iam access-groups --output json | jq -r ".[] | select(.id == \"$accessid\") | .name"`
          members=`ibmcloud iam access-group-users $access_name --output json | jq -r '.[].email'`
          serviceids=`ibmcloud iam access-group-service-ids $access_name --output json | jq -r .[].id`

      elif [ "`echo $accessid | cut -d'-' -f1`" = "iam" ] && [ "`echo $accessid | cut -d'-' -f2`" = "ServiceId" ]
      then
          accessType="Service ID"
          accessid=`echo $accessid | awk -F'iam' '{print substr($2,2)}'`
          access_name=`ibmcloud iam service-ids --output json | jq -r ".[] | select(.id == \"$accessid\") | .name"`

      elif [ "`echo $accessid | cut -d'-' -f1`" = "IBMid" ]
      then
          accessType="IBM User"
	  [ "$account_users" = "" ] && account_users="$(ibmcloud account users --output json)"
	  access_name=`echo $account_users | jq -r ".[] | select( .ibmUniqueId == \"$accessid\") | .userId")`

      fi

      echo "Access Type: $accessType"
      echo "Name: $access_name"
      echo "Id: $accessid"
      if [ "$accessType" = "Access Group" ]
      then
         echo Members:
         for member in $members; do echo "   $member"; done
         echo Service IDs:
         for iam_id in $serviceids
         do 
            serviceid=`echo $iam_id | awk -F'iam' '{print substr($2,2)}'`
            serviceid_name=`ibmcloud iam service-ids --output json | jq -r ".[] | select(.id == \"$serviceid\") | .name"`
            echo "   $serviceid: $serviceid_name"
         done
      fi

      echo Roles: $roles
      echo
   fi
done

echo Total number of policies found for bucket $bucket_name: $found_bucket_policies
echo ----------------------------------------------------------------------
