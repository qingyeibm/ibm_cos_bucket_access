

# Bucket Access

The script was written to answer the question: Who has access to a specific COS Bucket?

It reports all access types including the following:

1. Service IDs
2. Access Groups
3. IBM Users


## Tools used

1. IBM Cloud IAM Policy Management API
2. IBM Cloud CLI
3. jq - Command-line JSON processor


## Usage

- Log into the IBM Cloud with an 'ibmcloud login' command

- The command to run the script:  
     bucket_access <bucket_name>  

- Two environment variables need to be set before running the script:  
  1.  <your_account_id> IBMCLOUD_ACCOUNT_GUID  
  2.  <your_apikey>     IBMCLOUD_API_KEY  


## Example run

./bucket_access.sh cos-truata-lg3-cos-standard-cqr  

Number of policies found for COS instance 884c9ce4-fa34-4487-ac88-af5a76440ea5: 6  
List Access Policies for bucket: cos-truata-lg3-cos-standard-cqr  
Bucket access policy 1  
Access Type: Service ID  
Name: C101-Prod-COS-Buckets-SID  
Id: ServiceId-089074ef-5bc6-4280-95c9-34eaa08f7567  
Roles: Reader  

Bucket access policy 2  
Access Type: Service ID  
Name: qy-cos-test  
Id: ServiceId-43ffa9ad-a09b-4a4e-a008-961552924997  
Roles: Writer  

Bucket access policy 3  
Access Type: Access Group  
Name: GCAT-EU  
Id: AccessGroupId-b086eea9-6ffb-4b4a-8790-5465d816602a  
Members:  
   qing.ye@uk.ibm.com  
   ciaran.hennessy@ie.ibm.com  
Service IDs:  
   ServiceId-089074ef-5bc6-4280-95c9-34eaa08f7567: C101-Prod-COS-Buckets-SID  
Roles: Object Reader  

Bucket access policy 4  
Access Type: Access Group  
Name: C101-Prod-COS-AG  
Id: AccessGroupId-29bcf639-a782-471c-9e53-2f22e2d9156e  
Members:  
   qing.ye@uk.ibm.com  
   ciaran.hennessy@ie.ibm.com  
Service IDs:  
   ServiceId-089074ef-5bc6-4280-95c9-34eaa08f7567: C101-Prod-COS-Buckets-SID  
   ServiceId-43ffa9ad-a09b-4a4e-a008-961552924997: qy-cos-test  
   ServiceId-854f2a10-0eb2-4d77-a41e-29579b65c03f: C101-test  
Roles: Writer Content Reader Object Writer  

Total number of policies found for bucket cos-truata-lg3-cos-standard-cqr: 4  
